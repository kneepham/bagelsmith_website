<?php if ( !defined( 'ABSPATH' ) ) { exit; } ?>
<?php

/*
* Enqueue external scripts to add them to theme.
*/
function enqueueExternalScripts() {

	$scripts = array(
		// 'cssPlugin' 		=> '//cdnjs.cloudflare.com/ajax/libs/gsap/1.20.3/plugins/CSSPlugin.min.js',
		// 'scrolltoPlugin' 	=> '//cdnjs.cloudflare.com/ajax/libs/gsap/1.20.4/plugins/ScrollToPlugin.min.js',
		// 'easepack'			=> '//cdnjs.cloudflare.com/ajax/libs/gsap/1.20.3/easing/EasePack.min.js',
		// 'timelinelite' 		=> '//cdnjs.cloudflare.com/ajax/libs/gsap/1.20.4/TimelineLite.min.js',
		// 'tweenlite' 		=> '//cdnjs.cloudflare.com/ajax/libs/gsap/1.20.3/TweenLite.min.js',

		// 'slickSlidercss' => '//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css',
		// 'slickSliderjs' => '//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js',

		// 'owlcss' => '//cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css',
		// 'owldefaultcss' => '//cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css',
		// 'owljs' => '//cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js'
	);

	foreach($scripts as $key => $url){
		if (substr($key, -3) == 'css')
			wp_enqueue_style(
				$key, 
				$url, 
				array(), 
				'1.0'
			);
		else
			wp_enqueue_script(
				$key, 
				$url, 
				array(), 
				'1.0.1',
				true
			);
	}
}


add_action( 'wp_enqueue_scripts', 'enqueueAssets' , 999 );

/*
* Configuration setup to enqueue scripts and styles to the theme.
*/
function enqueueAssets() {
	enqueueLocalScripts();
	//enqueueExternalScripts();
}

/*
* Enqueue external scripts to add them to theme.
*/
function enqueueLocalScripts() {

	$files = array(
		'main-styles' 	=> '/resources/dist/css/main.css',
		'main-js' 	=> '/resources/dist/js/main.js',
	);

	foreach($files as $key => $path){
		if ($path) {

			if (substr($path, -2) == 'js') {
				wp_enqueue_script(
					$key, 
					get_template_directory_uri() . $path, 
					array('jquery'), 
					filemtime( get_template_directory() . $path), 
					true //Place after the Wordpress footer
				);

				wp_enqueue_script($key);
			} else {
				wp_enqueue_style(
					$key, 
					get_template_directory_uri() . $path, 
					array(), 
					filemtime( get_template_directory() . $path)
				);
			}

		}
	}

}