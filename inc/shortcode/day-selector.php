<?php if ( !defined( 'ABSPATH' ) ) { exit; } ?>
<?php

if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Woocommerce Shop Page Configuration',
		'menu_title'	=> 'WC Shop Config',
		'menu_slug' 	=> 'wc-shop-page-config',
		'capability'	=> 'edit_posts',
		'redirect'		=> false,
		'icon_url' 		=> 'dashicons-store'
	));
	
}


function getDaySelectorFields($key) {

	$default = '';

	if ($key == 'dc_daily_cutoff_time') {
		$default = '00:00:00';
		if ( class_exists('ACF') && get_field('dc_daily_cutoff_time','options') ) {
			$default = get_field('dc_daily_cutoff_time','options');
		}
	}

	if ($key == 'dc_weekly_operation_start_time') {
		$default = '00:00:00';
		if ( class_exists('ACF') && get_field('dc_weekly_operation_start_time','options') ) {
			$default = get_field('dc_weekly_operation_start_time','options');
		}
	}

	if ($key == 'dc_header_summary') {
		$default = 'Select a date for delivery.';
		if ( class_exists('ACF') && get_field('dc_header_summary','options') ) {
			$default = get_field('dc_header_summary','options');
		}
	}

	if ($key == 'dc_disclaimer') {
		$default = 'Our weekly inventory is loaded to the site every Sunday at 8pm.  Quantities are limited and can sell out.  Also, please be advised we only deliver inside the perimeter.';
		if ( class_exists('ACF') && get_field('dc_disclaimer','options') ) {
			$default = get_field('dc_disclaimer','options');
		}
	}

	if ($key == 'dc_error_message_1') {
		$default = 'You need to purchase products from the same day. Please remove items from different days from your cart to proceed.';
		if ( class_exists('ACF') && get_field('dc_error_message_1','options') ) {
			$default = get_field('dc_error_message_1','options');
		}
	}	

	return $default;			

}

function drawDaySelector($atts) {
	$html = "";

	if ( isset($atts['cat']) && $atts['cat'] ) {
		$cats = $atts['cat'];
		$cats = explode(',', $cats);
		$tz = 'America/Winnipeg';
		

		$cutoffTime = getDaySelectorFields('dc_daily_cutoff_time'); 
		$startTime = getDaySelectorFields('dc_weekly_operation_start_time');

		date_default_timezone_set($tz);

		$wc_options = get_option('woocommerce_permalinks');
		$product_category_base = isset($wc_options) && isset( $wc_options['category_base'] ) ? $wc_options['category_base'] : 'product-cagtegory';

		$today = time();

		$day = date('w');
		$week_start_date = date('Y-m-d ' . $startTime, strtotime('-'.$day.' days'));

		$wday = date('w', $today);

		$header = getDaySelectorFields('dc_header_summary');

		$days = array(
			//'sunday' 	=> date( 'Y-m-d ' . $cutoffTime, $today - ($wday - 0) * 86400 ), 
			//'monday' 	=> date( 'Y-m-d ' . $cutoffTime, $today - ($wday - 1) * 86400 ),
			'tuesday' 	=> date( 'Y-m-d ' . $cutoffTime, $today - ($wday - 2) * 86400 ),
			'wednesday' => date( 'Y-m-d ' . $cutoffTime, $today - ($wday - 3) * 86400 ),
			'thursday' 	=> date( 'Y-m-d ' . $cutoffTime, $today - ($wday - 4) * 86400 ),
			'friday' 	=> date( 'Y-m-d ' . $cutoffTime, $today - ($wday - 5) * 86400 ),
			'saturday' 	=> date( 'Y-m-d ' . $cutoffTime, $today - ($wday - 6) * 86400 )
		);

		$tz = 'America/Winnipeg';
		$timestamp = time();
		$dt = new DateTime("now", new DateTimeZone($tz)); 
		$dt->setTimestamp($timestamp);

		$today = $dt->format('Y-m-d H:i:s');

		if (is_array($cats) && count($cats) > 0) {

			$archiveCat = get_queried_object();
			$archiveCatId = isset($archiveCat) && $archiveCat && isset($archiveCat->term_id) ? $archiveCat->term_id : false;

			ob_start();
?>

<?php //echo var_dump($today > $week_start_date, $today, $week_start_date) ?>
<?php //var_dump($days) ?>
<div class="day-selector" data-wc-cpt-tax-slug="<?php echo $product_category_base ?>">
	<div class="day-selector__container">
		<div class="day-selector__header">
			<div class="day-selector__header-title">
				<h2><?php echo $header ?></h2>
			</div>
		</div>
		<div class="day-selector__items">
			<?php foreach ($cats as $key => $cat): ?>

				<?php 
					$term = get_term($cat, 'product_cat');
					$day = isset($days[$term->slug]) ? $days[$term->slug] : false;
					$available = $today > $day || $today < $week_start_date  ? true : false;
				?>
				<div class="day-selector__item <?php if ($available): ?>day-selector__item--disabled<?php endif ?>">
					<a data-day-key="<?php echo $term->slug ?>" href="<?php echo !$available ? get_term_link($term) : '' ?>">
						<?php echo $term->name ?>
					</a>
				</div>
			<?php endforeach ?>
		</div>

		<select id="day-selector-select">
			<?php foreach ($cats as $key => $cat): ?>

				<?php 
					$term = get_term($cat, 'product_cat');
					$day = isset($days[$term->slug]) ? $days[$term->slug] : false;
					$available = $today > $day || $today < $week_start_date ? true : false;
				?>
				<option 
					value="<?php echo !$available ? get_term_link($term) : '' ?>"
					data-disabled="<?php if ($available): ?>day-selector__item--disabled<?php endif ?>"
					data-day-key="<?php echo $term->slug ?>" href="<?php echo !$available ? get_term_link($term) : '' ?>">
						<?php echo $term->name ?>
				</option>
			<?php endforeach ?>
		</select>		

		<div class="day-selector__day-items">
			<?php foreach ($days as $key => $day): ?>
				<div data-target-day-key="<?php echo $key ?>" class="day-selector__day-item">
					
					<div class="day-selector__day-item-availability">
						<?php if ( $today < $day && $today > $week_start_date): ?>
							
						<?php else: ?>
							<div class="day-selector__box">
								<?php echo getDaySelectorFields('dc_disclaimer') ?>
							</div>
							<br>
							<?php _e('Unavailable') ?>
						<?php endif ?>
					</div>
					<div class="day-selector__day-item-text"><?php echo date('l - F d, Y', strtotime($day) ) ?></div>
			
				</div>
			<?php endforeach ?>
		</div>

		<div class="day-selector__products-loading">
			  <i class="fas fa-circle-notch fa-spin"></i>
			  ...Loading Products
		</div>

		<div class="day-selector__products ">
		</div>
	</div>
</div>

<?php
			$html = ob_get_clean();

		}
	}
	
	return $html;
}

function validate_all_cart_contents(){

	$days = array(
		'sunday',
		'monday',
		'tuesday',
		'wednesday',
		'thursday',
		'friday',
		'saturday'
	);

	$master_terms = array();

	if ( WC()->cart->cart_contents ) {
	    foreach ( WC()->cart->cart_contents as $cart_content_product ) {
	 	
	    	$product_id = $cart_content_product['product_id'];

	    	if ($product_id) {
		    	$terms = get_the_terms($product_id,'product_cat');

		    	if ($terms) {
			    	foreach($terms as $term) {
			    		if (in_array($term->slug, $days)) {

			    			if (isset($term->slug) && $term->slug)
				    			$master_terms[$term->slug] = $term->slug;

				    	}
			    	}
			    }
		    }
	    }
	}

	if (count($master_terms) > 1) {
		wc_add_notice( sprintf( getDaySelectorFields('dc_error_message_1') ), 'error' );
	} else {
		return true;
	}
}

///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////

add_shortcode( 'DaySelector', 'drawDaySelector' );
add_action('woocommerce_check_cart_items', 'validate_all_cart_contents');